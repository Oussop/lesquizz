import headers from "../../config.js";

export default {
    view: async () => {
        return `
    <div class="blackCircle"><img src="../../assets/img/background.svg"  alt="image de fond"></div>

    <div class="imgAlbumSlider">
        <img class="firstAlbumSlider" src="../../assets/img/boobaCut.jpg" alt="album de booba">
        <img class="secondAlbumSlider" src="../../assets/img/medine.jpg" alt="album de medine">
        <img class="thirdAlbumSlider" src="../../assets/img/sch.jpg" alt="album de sch">
        <img class="fourthAlbumSlider" src="../../assets/img/liltjayCut.jpg" alt="album de liltjay">
        
        <div class="logoLeSquizzz">
        <h1>Le Squizzz</h1>
        <a href=""><img src="../../assets/img/logoLeSquizz.png" alt="Logo le squizz"></a></div>
    </div>
    
    <div class="containerSlider">
        <div class="slider">
            <div class="leftContainerDifficulty">
                <img src="../../assets/img/Kacem.svg" alt="Album de kacem">
            </div>
            <hr class="hrMiddle">
            <div class="rightContainerDifficulty">
                <div class="rules">
                    <h2>Règles du jeu</h2>
                    
                    <div><p>Répondez aux 9 questions de ce quiz,
                        à chaque bonne réponse une case de notre étoile mystérieuse sera révélé.
                        Tentez de découvrir quelle pochette d'album se cache derriere notre étoile
                            mystérieuse pour gagner 5 points supplémentaire.
                    </div></p>
                   
                </div>
                    <hr>

                    <div class="difficultyParent">
                        <div class="difficulty"><a href="#quiz"><button value="easy" class="difficult">EASY</button></a></div>
                        <div class="difficulty"><a href="#quiz"><button value="medium" class="difficult">MEDIUM</button></a></div>
                        <div class="difficulty"><a href="#quiz"><button value="hard" class="difficult">HARD</button></a></div>
                    </div>
            </div>
        </div>
    </div>
`
    },
    after: () => {
        console.log("Component About mounted");
        let tags = localStorage['tags']

        let optionElement = document.getElementsByClassName('difficult')

        for (const optionElementElement of optionElement) {
            optionElementElement.addEventListener('click', async() => {

                const ENDPOINT = "https://quizapi.io/api/v1/questions";

                localStorage.setItem('difficulty',optionElementElement.value)
                let difficulty = localStorage['difficulty']

                    const breakPoint = '?limit=9' + '&category=' + tags + '&difficulty=' + difficulty;

                        let response = await fetch(ENDPOINT + breakPoint, {
                            method: 'GET', headers,

                        }).then(function (response) {
                            return response.json();

                        }).then(function (body) {
                            return body;
                        })
                // console.log(response)
                localStorage.setItem('response', JSON.stringify(response))
            })
        }
    }
};
