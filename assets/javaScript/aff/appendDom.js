
function appendDom (localStorage){
    let number = parseInt(localStorage.getItem('number')) ;
    let response = JSON.parse(localStorage.getItem('response'))
    let questionElement = document.getElementById('question')
    let containerResponseQuizzElement = document.getElementById('containerResponseQuizz')

    questionElement.innerText = response[number]['question']
    if(number === 0){
        for(const up in response[number]['answers']){

                let conteneurResponse = document.createElement('div')
                conteneurResponse.classList.add('answerLeSquizz')

                let contenuResponse = document.createElement('span')

                contenuResponse.classList.add('answer')
                contenuResponse.setAttribute("id",up)
                contenuResponse.innerText = (response[number]['answers'][up])
                conteneurResponse.appendChild(contenuResponse)
                containerResponseQuizzElement.appendChild(conteneurResponse)
        }
        let contenuResponse = document.getElementsByClassName('answer')

        for(const responseChoice of contenuResponse){
            for(const up in response[number]['answers']){
                contenuResponse[up].innerText = (response[number]['answers'][up])
            }
            if(responseChoice.innerText === "") {
                responseChoice.style.display = 'none'
            }else{
                responseChoice.style.display = "flex"
            }
        }


    }else{
        let contenuResponse = document.getElementsByClassName('answer')

        for(const responseChoice of contenuResponse){
            for(const up in response[number]['answers']){
                    contenuResponse[up].innerText = (response[number]['answers'][up])
            }
            if(responseChoice.innerText === "") {
                responseChoice.style.display = 'none'
            }else{
                responseChoice.style.display = "flex"
            }
        }
    }
}


export default appendDom;