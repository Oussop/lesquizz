import response from "./Difficulty.js";
import appendDom from "./aff/appendDom.js";


export default {
    view: async () => {
        return `
<div id="block" class="container">
        <ul class="loader">
            <li class="animatedLogo"></li>
            <li class="animatedLogo"></li>
            <li class="animatedLogo"></li>
            <li class="animatedLogo"></li>
            <li class="animatedLogo"></li>
            <li class="animatedLogo"></li>
        </ul>
</div>
    

    <div class="sidebar">
    <div class="buttonSidebar" id="buttonSidebar"><img src="../../assets/img/rightArrow.png" alt="flèche de droite">
        </div>
       
        <div class="parentGrid">
            <div class="personality">
                <img src="../../assets/img/booba.jpg" alt="Album de medine">
            </div>
            <div class="grid">
                <div class="grid0"></div>
                <div class="grid1"></div>
                <div class="grid2"></div>
                <div class="grid3"></div>
                <div class="grid4"></div>
                <div class="grid5"></div>
                <div class="grid6"></div>
                <div class="grid7"></div>
                <div class="grid8"></div>
            </div>
            
        </div>
        <div id="containerResponseAlbum" class="containerResponseSidebar">
            <div class="responseSidebar">
                <input id="responseAlbum" class="responseAlbum" type="text">
                <input id="buttonAlbum" class="buttonAlbum" type="button" value="valider">
            </div>
        </div>
    </div>

<div class="logoLeSquizzz">
        <h1>Le Squizzz</h1>
        <a href=""><img src="../../assets/img/logoLeSquizz.png" alt="Logo le squizz"></a></div>
    
    <form class="pictureAlbumQuiz">
    
        <li class="firstAlbumQuiz">
            <img src="../../assets/img/liveLoveAsap.jpg" alt="Album de Asap">
         
        </li>
        <li class="fourAlbumQuiz">
            <img src="../../assets/img/gazo.jpg" alt="Album de Gazo">
            <img src="../../assets/img/kendrickLamar.jpg" alt="Album de Kendrick">
        </li>
    
    </form>
    
    <div class="containerQuiz">
        <div class="questionQuiz">
            <div class="containerQuestion">
                <span id="question" class="questionLeSquizz"</span>
                                                                             
            </div>  
        </div>
    
        <div id="containerResponseQuizz" class="containerAnswer">
                       
        </div>
    </div>
    
`
    },
    after: async () => {
        console.log("Component Home mounted");

        let blockLoading = document.getElementById('block')
        window.addEventListener('animationiteration', () => {
            setTimeout(() =>{
                blockLoading.style.display = 'none'},1500)})

        let buttonSidebarElement = document.getElementsByClassName('buttonSidebar')[0];
        let sidebarElement = document.getElementsByClassName('sidebar')[0];

        buttonSidebarElement.addEventListener('click', () => {
            sidebarElement.classList.toggle("openSidebar")
            buttonSidebarElement.classList.toggle("rotateButton")
        })

        let number = 0;
        localStorage.setItem("number", number)
        let result = 0;
        localStorage.setItem('result', result)
        appendDom (localStorage);


        let response = JSON.parse(localStorage.getItem('response'))
console.log(number)

        let buttonResponse = document.getElementsByClassName("answer")
        for(const buttonResponseResponse  of buttonResponse) {
            buttonResponseResponse.addEventListener('click', async() => {
                let number = parseInt(localStorage.getItem('number'))
                number++
                localStorage.setItem('number', number)

                if(number < 9){
                    if(buttonResponseResponse.id === response[number-1]['correct_answer']){
                        let result = parseInt(localStorage.getItem('result'))
                        result++
                        localStorage.setItem('result', result)
                            buttonResponseResponse.style.backgroundColor = '#21CB13'
                            setTimeout(() => {
                                buttonResponseResponse.style.backgroundColor = ''
                                sidebarElement.classList.add("openSidebar")
                                buttonSidebarElement.classList.toggle("rotateButton")
                                setTimeout( ()=>{appendDom (localStorage); },700)

                            }, 500)

                        let node = document.querySelector('.grid');
                        let gridLength = node.children.length;
                        let randomGrid = "grid" + Math.floor(Math.random() * gridLength)
                        let grid_ = document.querySelector(`.${randomGrid}`)
                        grid_.style.backgroundColor = 'transparent'


                    }else if(buttonResponseResponse.id !== response[number-1]['correct_answer'])
                    {
                        buttonResponseResponse.style.backgroundColor = '#C51818'
                        setTimeout(() => {
                        buttonResponseResponse.style.backgroundColor = ''
                            setTimeout( ()=>{appendDom (localStorage); },700)
                    }, 1500)
                    }

                }else if(number === 9){
                    if(buttonResponseResponse.id === response[number-1]['correct_answer']){

                        appendDom (localStorage);
                        let result = parseInt(localStorage.getItem('result'))
                        result++
                        localStorage.setItem('result', result)
                    }
                        let containerResponseAlbumElement =document.getElementById('containerResponseAlbum')
                        sidebarElement.classList.add("openSidebar")

                        containerResponseAlbumElement.style.display = 'flex'
                        buttonSidebarElement.style.display = 'none'

                    }
                })
        }
        let nameAlbum = "";

        let responseAlbumElement = document.getElementById('responseAlbum').value
        let buttonAlbumElement = document.getElementById('buttonAlbum')
        buttonAlbumElement.addEventListener('click', ()=>{
            if(responseAlbumElement === nameAlbum ){
                let number = parseInt(localStorage.getItem('number'))
                number+6
                localStorage.setItem('number', number)
                window.location.href = '#score'

            }else{
                console.log('toto')
                window.location.href = '#score'
            }
        })

    }
};
