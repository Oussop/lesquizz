
export default {
    view: async () => {
        return `
<div>
    <div class="blackCircle"><img src="../../assets/img/background.svg"  alt="image de fond"></div>
    <div class="groupScore">
        <span class="titleScore">
            <h2>Your Score</h2>
        </span>
        <span class="containerScore">
            <p class="numberScore"><span id="countNumberScore">10</span>/15 pts</p>
        </span>
        <span>
            <p id="compliment"></p>
        </span>
    </div>
    <div class="buttonContainer">
        <a  href=""><div id="buttonWelcome" class="buttonWelcome">Retour a l'accueil</div></a>
    </div>
</div>`
    },
    after: () => {
        console.log("Component Home mounted");
        let score = parseInt(localStorage.getItem('result'))
        let scoreElement = document.getElementById('countNumberScore')
        scoreElement.innerText = score
        let complimentElement = document.getElementById('compliment')

        if(score <= 8){
            complimentElement.innerText = 'va travailler gros shlag!!!'
        }else{
            complimentElement.innerText = 'C\'est bien continue sur ta lancé'
        }

    }
};
