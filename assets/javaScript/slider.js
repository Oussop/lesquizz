
export default {
    view: async () => {
        return `

    <section id="welcomePage" class="loaderHome">
        <div class="firstCircle">
            <img src="../../assets/img/firstCircleWhite.svg" alt="firstCircleWhite">
        </div>
        
        <div class="secondCircle">
            <img src="../../assets/img/secondCircleWhite.svg" alt="secondCircleWhite">
        </div>
        
        <form class="pictureAlbum">
            <li class="firstAlbum">
                <img src="../../assets/img/liveLoveAsap.jpg" alt="Album de Asap">
            </li>
            <li class="secondAlbum">
                <img src="../../assets/img/kendrickLamar.jpg" alt="Album de Kendrick">
            </li>
            <li class="thirdAlbum">
                <img src="../../assets/img/kaaris.jpg" alt="Album de Kaaris">
            </li>
            <li class="fourAlbum">
                <img src="../../assets/img/gazo.jpg" alt="Album de Gazo">
            </li>
        </form>
        
        
        <div class="titleMain">
            <div class="title">
                <h1>Le Squizzz</h1>
                <img src="../../assets/img/logoLeSquizz.png" alt="Logo le squizz">
            </div>  
            <div>
                <button id="buttonWelcome" class="buttonSwitchSlide">SQUEZZOS</button>
            </div>
        </div>
    </section>
    
    
        <div class="blackCircle"><img src="../../assets/img/background.svg" alt="image"></div>

        <div class="imgAlbumSlider">
        <img class="firstAlbumSlider" src="../../assets/img/boobaCut.jpg" alt="album de booba">
        <img class="secondAlbumSlider" src="../../assets/img/medine.jpg" alt="album de medine">
        <img class="thirdAlbumSlider" src="../../assets/img/sch.jpg" alt="album de sch">
        <img class="fourthAlbumSlider" src="../../assets/img/liltjayCut.jpg" alt="album de liltjay">
        
        <div class="logoLeSquizzz">
        <h1>Le Squizzz</h1>
        <a href=""><img src="../../assets/img/logoLeSquizz.png" alt=""></a></div>
    </div>
        <div class="titleContainer">
            <h1>Choisissez votre catégorie</h1></div>
        <div class="containerSlider1">
            <div class="containerContentSlider">
            <div class="prevArrowSlider"><img src="../../assets/img/prevArrow.png" alt="flèche précédent"></div>
                                <div class="slider1">
                                    <div class="mainContainerDifficulty1 active">
                                    <div class="leftContainerDifficulty1">
                                        <div class="content">
                                            <h2>Quiz <br> linux</h2>
                                        </div>
        
                                    </div>
                                    <hr class="hrMiddle">
                                    <div class="rightContainerDifficulty1">
                                        <div class="rules1">
                                            <h2>Règles du jeu</h2>
                                            <span>Répondez aux 9 questions de ce quiz,
                                                    à chaque bonne réponse une case de notre étoile mystérieuse sera révélé.
                                                    Tentez de découvrir quelle pochette d'album se cache derriere notre étoile
                                                    mystérieuse pour gagner 5 points supplémentaire.
                                                </span>
                                        </div>
                                        <hr>
                                        <div class="buttonCategory">
                                           <a href="#difficulty"><button value="Linux" class="difficulty1">Choisir cette catégorie</button></a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="mainContainerDifficulty1">
                                        <div class="leftContainerDifficulty1">
                                            <div class="content">
                                                <h2>Quiz <br> devops</h2>
                                            </div>
        
                                        </div>
                                        <hr class="hrMiddle">
                                        <div class="rightContainerDifficulty1">
                                            <div class="rules1">
                                                <h2>Règles du jeu</h2>
                                                <span>Répondez aux 9 questions de ce quiz,
                                                    à chaque bonne réponse une case de notre étoile mystérieuse sera révélé.
                                                    Tentez de découvrir quelle pochette d'album se cache derriere notre étoile
                                                    mystérieuse pour gagner 5 points supplémentaire.
                                                </span>
                                            </div>
                                            <hr>
                                            <div class="buttonCategory">
                                                <a href="#difficulty"><button value="Devops" class="difficulty1">Choisir cette catégorie</button></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mainContainerDifficulty1">
                                        <div class="leftContainerDifficulty1">
                                            <div class="content">
                                                <h2>Quiz <br> networking</h2>
                                            </div>
        
                                        </div>
                                        <hr class="hrMiddle">
                                        <div class="rightContainerDifficulty1">
                                            <div class="rules1">
                                                <h2>Règles du jeu</h2>
                                                <span>Répondez aux 9 questions de ce quiz,
                                                    à chaque bonne réponse une case de notre étoile mystérieuse sera révélé.
                                                    Tentez de découvrir quelle pochette d'album se cache derriere notre étoile
                                                    mystérieuse pour gagner 5 points supplémentaire.
                                                </span>
                                            </div>
                                            <hr>
                                            <div class="buttonCategory">
                                                <a href="#difficulty"><button value="Networking" class="difficulty1">Choisir cette catégorie</button></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mainContainerDifficulty1">
                                        <div class="leftContainerDifficulty1">
                                            <div class="content">
                                                <h2>Quiz <br> programming</h2>
                                            </div>
        
                                        </div>
                                        <hr class="hrMiddle">
                                        <div class="rightContainerDifficulty1">
                                            <div class="rules1">
                                                <h2>Règles du jeu</h2>
                                                <span>Répondez aux 9 questions de ce quiz,
                                                    à chaque bonne réponse une case de notre étoile mystérieuse sera révélé.
                                                    Tentez de découvrir quelle pochette d'album se cache derriere notre étoile
                                                    mystérieuse pour gagner 5 points supplémentaire.
                                                </span>
                                            </div>
                                            <hr>
                                            <div class="buttonCategory">
                                                <a href="#difficulty"><button value="Programming" class="difficulty1">Choisir cette catégorie</button></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mainContainerDifficulty1">
                                        <div class="leftContainerDifficulty1">
                                            <div class="content">
                                                <h2>Quiz <br> cloud</h2>
                                            </div>
        
                                        </div>
                                        <hr class="hrMiddle">
                                        <div class="rightContainerDifficulty1">
                                            <div class="rules1">
                                                <h2>Règles du jeu</h2>
                                                <span>Répondez aux 9 questions de ce quiz,
                                                    à chaque bonne réponse une case de notre étoile mystérieuse sera révélé.
                                                    Tentez de découvrir quelle pochette d'album se cache derriere notre étoile
                                                    mystérieuse pour gagner 5 points supplémentaire.
                                                </span>
                                            </div>
                                            <hr>
                                            <div class="buttonCategory">
                                                <a href="#difficulty"><button value="Cloud" class="difficulty1">Choisir cette catégorie</button></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mainContainerDifficulty1">
                                        <div class="leftContainerDifficulty1">
                                            <div class="content">
                                                <h2>Quiz <br> docker</h2>
                                            </div>
        
                                        </div>
                                        <hr class="hrMiddle">
                                        <div class="rightContainerDifficulty1">
                                            <div class="rules1">
                                                <h2>Règles du jeu</h2>
                                                <span>Répondez aux 9 questions de ce quiz,
                                                    à chaque bonne réponse une case de notre étoile mystérieuse sera révélé.
                                                    Tentez de découvrir quelle pochette d'album se cache derriere notre étoile
                                                    mystérieuse pour gagner 5 points supplémentaire.
                                                </span>
                                            </div>
                                            <hr>
                                            <div class="buttonCategory">
                                                <a href="#difficulty"><button value="Docker" class="difficulty1">Choisir cette catégorie</button></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mainContainerDifficulty1">
                                        <div class="leftContainerDifficulty1">
                                            <div class="content">
                                                <h2>Quiz <br> kubernetes</h2>
                                            </div>
        
                                        </div>
                                        <hr class="hrMiddle">
                                        <div class="rightContainerDifficulty1">
                                            <div class="rules1">
                                                <h2>Règles du jeu</h2>
                                                <span>Répondez aux 9 questions de ce quiz,
                                                    à chaque bonne réponse une case de notre étoile mystérieuse sera révélé.
                                                    Tentez de découvrir quelle pochette d'album se cache derriere notre étoile
                                                    mystérieuse pour gagner 5 points supplémentaire.
                                                </span>
                                            </div>
                                            <hr>
                                            <div class="buttonCategory">
                                                <a href="#difficulty"><button value="kubernetes" class="difficulty1">Choisir cette catégorie</button></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                <div class="nextArrowSlider"><img src="../../assets/img/nextArrow.png" alt="flèche suivant"></div>
            </div>
        </div>
`
    },
    after: () => {
        console.log("Component Home mounted");
        let buttonWelcomeLoad = document.getElementById('buttonWelcome')
        let elementWelcomeLoad = document.getElementById('welcomePage')
        buttonWelcomeLoad.addEventListener('click',
            (() => {
                elementWelcomeLoad.classList.add('loading')
            } ))

        let mainContainerDifficulty1 = document.getElementsByClassName('mainContainerDifficulty1');

        let step = 0

        let numberMainContainerDifficulty1 = mainContainerDifficulty1.length

        let prevArrowSlider = document.querySelector('.prevArrowSlider')
        let nextArrowSlider = document.querySelector('.nextArrowSlider')



        function getOfActiveMainContainerDifficulty1() {
            for(let i= 0 ; i < numberMainContainerDifficulty1; i++) {
                  mainContainerDifficulty1[i].classList.remove('active')
            }
        }

        nextArrowSlider.addEventListener('click', function () {
            step++
            if (step >=numberMainContainerDifficulty1){
                step = 0
            }
            getOfActiveMainContainerDifficulty1()
            mainContainerDifficulty1[step].classList.add('active')
        })

        console.log(nextArrowSlider)
        prevArrowSlider.addEventListener('click', function () {
            step--
            if (step < 0){
                step = numberMainContainerDifficulty1 -1
            }
            getOfActiveMainContainerDifficulty1()
            mainContainerDifficulty1[step].classList.add('active')
        })
        let tagsElement = document.getElementsByClassName('difficulty1')

        for (const tagsElementElement of tagsElement) {
            tagsElementElement.addEventListener('click', async (e) => {

                localStorage.setItem("tags", tagsElementElement.value)

            })
        }}
    };
