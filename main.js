import 'normalize.css'
import '/style.css'

import ROUTER from "./router.js";

window.addEventListener('load', async () => {
    await ROUTER();
});
